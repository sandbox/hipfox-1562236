Webform component clone (WFCC) is a webform module widget, it provides a clone
across the different webforms component.

If you have many different webforms (already existing), but some very similar to
the webform component, then the module will be suitable for use, you can easy
clone, modify component and save new one.

-- Features --
* This module is an add-on module for webform.
* Autocomplete component search, across the webforms.
* Clone an existing component and save new one.

-- Requirements --
* webform (6.x-3.x)

-- Installation --
* Install as usual, see http://drupal.org/node/70151 for further information.

-- Usage --
* Locate edit path like node/*/webform.
* Goto Autocomplete component clone field.
* Type something to search other components.
* Hit "Component Clone" button.
* Re-edit and save it.
* Done, the clone component belong to the new one.

-- Contact --
Current maintainer:
* Chen-Sheng Lin (Hipfox) - http://drupal.org/user/62337
